# Bootstrap flag icons

Provides a Bootstrap 5 dropdown button to switch between available languages
use cdn from https://flagicons.lipis.dev/
Best use with [bootstrap 5 admin theme](https://www.drupal.org/project/bootstrap5_admin)

For a full description of the module, visit the
[project page](https://www.drupal.org/project/bootstrap_flag_icons).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/bootstrap_flag_icons).


## Table of contents

- Installation
- Configuration
- Maintainers


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no modifiable settings. There is no configuration.

## How to use

After install you can place block Bootstrap Language switcher.


## Maintainers

- NGUYEN Bao - [lazzyvn](https://www.drupal.org/u/lazzyvn)
