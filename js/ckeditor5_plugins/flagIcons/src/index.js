/**
 * @module flag-icons
 */

import FlagIcons from './FlagIcons';

export default {
  FlagIcons,
};
